{-# LANGUAGE ForeignFunctionInterface #-}
module M where
import System.IO
import Control.Concurrent (threadDelay, forkIO)
import Foreign.C.Types ( CInt(..) )

foreign export ccall haskellFunction :: CInt -> IO CInt

haskellFunction :: CInt -> IO CInt
haskellFunction x = do
  mainX
  pure $ x + 42

mainX :: IO ()
mainX = do
  -- Create three asynchronous IO actions
  let actions = [action 1000000000000000000000000000000, action 2, action 3]

  -- Start the actions concurrently
  mapM_ forkIO actions

  -- Print the results
  putStrLn "Results:"

-- An example IO action that performs some work
action :: Integer -> IO ()
action n = do
  hPutStrLn stderr $ show n
  action (n + 1)
