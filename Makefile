main: main.c M.hs
	/usr/bin/ghc -threaded -o m.o -c M.hs
	/usr/bin/ghc -no-hs-main -threaded -o main main.c m.o

clean:
	rm -f *.hi *.o *.so main
