#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "Rts.h"

extern int haskellFunction(int);

char * release_me;

__attribute__((destructor))
void sleep_some_1() {
  int x = write(2, "sleep_some_1\n", strlen("sleep_some_1\n"));
  hs_exit();
  sleep(1);
}

__attribute__((destructor))
void sleep_some_2() {
  free(release_me);
  int x = write(2, "sleep_some_2\n", strlen("sleep_some_2\n"));
  sleep(1);
  kill(getpid(), SIGINT);
  printf("Self\n");
}


void init_c() {
  fprintf(stderr, "init_c\n");

  char prog_name[] = "test";
  char *fake_args[] = {prog_name, NULL};
  char **fake_argv = fake_args;
  int fake_argc = 1;
  hs_init(&fake_argc, &fake_argv);

  fprintf(stderr, "init_c_done\n");
}

int main() {
  release_me = malloc(10);
  init_c();
  int result = haskellFunction(10);
  printf("Result from Haskell: %d\n", result);
  printf("Once\n");
  sleep(1);
  printf("Twice\n");
  return 0;
}
